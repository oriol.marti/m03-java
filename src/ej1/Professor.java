package ej1;

public class Professor extends Empleat {
    private String cicle;
    private String horari;

    public Professor(String nom, String cognom, String NIF, int sou, String cicle, String horari) {
        super(nom, cognom, NIF, sou);
        this.cicle = cicle;
        this.horari = horari;
    }

    public String getCicle() {
        return cicle;
    }

    public void setCicle(String cicle) {
        this.cicle = cicle;
    }

    public String getHorari() {
        return horari;
    }

    public void setHorari(String horari) {
        this.horari = horari;
    }

    @Override
    public String toString() {
        return "Professor{" +
                "cicle='" + cicle + '\'' +
                ", horari='" + horari + '\'' +
                '}';
    }
}
