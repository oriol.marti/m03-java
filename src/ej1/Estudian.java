package ej1;

public class Estudian extends Persona {
    private String cicle;
    private int curs;

    public Estudian(String nom, String cognom, String NIF, String cicle, int curs) {
        super(nom, cognom, NIF);
        this.cicle = cicle;
        this.curs = curs;
    }

    public String getCicle() {
        return cicle;
    }

    public void setCicle(String cicle) {
        this.cicle = cicle;
    }

    public int getCurs() {
        return curs;
    }

    public void setCurs(int curs) {
        this.curs = curs;
    }

    @Override
    public String toString() {
        return "Estudian{" +
                "nom '" + super.getNom() + '\'' +
                "cognom '" + super.getCognom() + '\'' +
                "NIF '" + super.getNIF() + '\'' +
                "cicle='" + cicle + '\'' +
                ", curs=" + curs +
                '}';
    }
}
