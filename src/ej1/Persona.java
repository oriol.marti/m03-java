package ej1;

public class Persona {
    private String nom;
    private String cognom;
    private String NIF;

    public Persona(String nom, String cognom, String NIF) {
        this.nom = nom;
        this.cognom = cognom;
        this.NIF = NIF;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCognom() {
        return cognom;
    }

    public void setCognom(String cognom) {
        this.cognom = cognom;
    }

    public String getNIF() {
        return NIF;
    }

    public void setNIF(String NIF) {
        this.NIF = NIF;
    }

    @Override
    public String toString() {
        return "Persona{" +
                "nom='" + nom + '\'' +
                ", cognom='" + cognom + '\'' +
                ", NIF='" + NIF + '\'' +
                '}';
    }
}
