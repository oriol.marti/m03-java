package ej1;

public class Empleat extends Persona {
    private int sou;

    public Empleat(String nom, String cognom, String NIF, int sou) {
        super(nom, cognom, NIF);
        this.sou = sou;
    }

    public int getSou() {
        return sou;
    }

    public void setSou(int sou) {
        this.sou = sou;
    }

    @Override
    public String toString() {
        return "Empleat{" +
                "sou=" + sou +
                '}';
    }
}
