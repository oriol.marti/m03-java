package ejabstracte1;

public class Mostrador extends Empleat {
    private int vendes;

    public Mostrador(String nom, String ciutatorigen, String lloc, int vendes) {
        super(nom, ciutatorigen, lloc);
        this.vendes = vendes;
    }

    @Override
    public int salariDiari() {
        this.vendes = 50;
        int porcentaje = this.vendes * 15 / 100;
        return porcentaje+this.vendes;
    }
}
