package ejabstracte1;

public abstract class Empleat {

    private String nom;
    private String ciutatorigen;
    private String lloc;

    public Empleat(String nom, String ciutatorigen, String lloc) {
        this.nom = nom;
        this.ciutatorigen = ciutatorigen;
        this.lloc = lloc;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCiutatorigen() {
        return ciutatorigen;
    }

    public void setCiutatorigen(String ciutatorigen) {
        this.ciutatorigen = ciutatorigen;
    }

    public String getLloc() {
        return lloc;
    }

    public void setLloc(String lloc) {
        this.lloc = lloc;
    }
    public abstract int salariDiari();

}
