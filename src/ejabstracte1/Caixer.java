package ejabstracte1;

import javax.swing.text.Document;

public class Caixer extends Empleat{

    private int HoresTreballades;

    public Caixer(String nom, String ciutatorigen, String lloc, int horesTreballades) {
        super(nom, ciutatorigen, lloc);
        HoresTreballades = horesTreballades;
    }

    public int getHoresTreballades() {
        return HoresTreballades;
    }

    public void setHoresTreballades(int horesTreballades) {
        HoresTreballades = horesTreballades;
    }

    @Override
    public int salariDiari() {
        return this.HoresTreballades*15;
    }
}
