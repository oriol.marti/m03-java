package ejabstracte1;

import org.w3c.dom.Document;

public class Neteja extends Empleat{

    public Neteja(String nom, String ciutatorigen, String lloc) {
        super(nom, ciutatorigen, lloc);
    }

    @Override
    public int salariDiari() {
        return 35;
    }
}
