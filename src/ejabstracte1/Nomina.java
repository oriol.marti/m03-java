package ejabstracte1;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

public class Nomina{
    private ArrayList<Empleat> empleats = new ArrayList<>();

    public Nomina() {
    }

    public void inserixEmpleat(Empleat e) {
        this.empleats.add(e);
    }
    public void eliminaNeteja(){
        Consumer<Empleat> eExpresion = Item -> {
            if (Item.getClass().equals("Neteja")) {
                this.empleats.remove(Item);
            }};
        this.empleats.forEach(eExpresion);
    }
    public int quantsCornella(){
        AtomicInteger contador = new AtomicInteger();
        Consumer<Empleat> eLloc = Item -> {
            if (Item.getLloc().equals("Cornella")) {
                contador.getAndIncrement();
            }
        };
        this.empleats.forEach(eLloc);
        return contador.get();
    }
    public int costNomita(){
        int n = 0;
        int[] total = new int[n + 1];

        Consumer<Empleat> eTotalNomina = Item ->{
            total[n] =  Item.salariDiari();
        };
        this.empleats.forEach(eTotalNomina);
        return Arrays.stream(total).sum();
    }
    public int quantitatCaixeres(){
        AtomicInteger total = new AtomicInteger();

        Consumer<Empleat> qcaixers = Item -> {
            if (Item.getClass().getName() == "Caixer"){
                total.getAndIncrement();
            }
        } ;

        this.empleats.forEach(qcaixers);

        return total.get();
    }
    public double souPromig(){
        int n = 0;
        double media;
        int[] promig = new int[n + 1];

        Consumer<Empleat> eTotalNomina = Item ->{
            promig[n] =  Item.salariDiari();
        };
        this.empleats.forEach(eTotalNomina);

        media = (double)this.costNomita() / promig.length;

        return media;

    }

}
