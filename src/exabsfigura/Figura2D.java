package exabsfigura;

import exabsfigura.Figura;

public abstract class Figura2D extends Figura {

    public abstract int calculaArea();

    public abstract int calculaPerimetre();

}
