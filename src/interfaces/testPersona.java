package interfaces;

import java.util.*;

public class testPersona {
    public static void main(String[] args) {
        ArrayList<Persona>pr=new ArrayList<Persona>();
        pr.add(new Persona("Dani", 22, 187));
        pr.add(new Persona("Pol", 52, 173));
        pr.add(new Persona("Manel", 27, 158));
        pr.add(new Persona("David", 25, 164));
        pr.add(new Persona("Pere", 80, 184));

        Collections.sort(pr);
        for(Persona persona:pr){
            System.out.println(persona.toString());
        }
    }

}
