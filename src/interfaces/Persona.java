package interfaces;

import java.util.List;

public class Persona implements Comparable<Persona> {
    private String nom;
    private int edat;
    private int alçada; //cm

    public Persona(String nom, int edat, int alçada) {
        this.nom = nom;
        this.edat = edat;
        this.alçada = alçada;
    }

    public int getEdat() {
        return edat;
    }

    public void setEdat(int edat) {
        this.edat = edat;
    }

    public int getAlçada() {
        return alçada;
    }

    public void setAlçada(int alçada) {
        this.alçada = alçada;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Persona{" +
                "nom='" + nom + '\'' +
                ", edat=" + edat +
                ", alçada=" + alçada +
                '}';
    }

    @Override
    public int compareTo(Persona persona) {
        if(alçada == persona.alçada)
            return 0;
        else if (alçada< persona.alçada)
            return 1;
        else
            return -1;
    }

}



