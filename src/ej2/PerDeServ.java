package ej2;

public class PerDeServ extends Empleat {
    private String seccio;

    public PerDeServ(String nom, String cognom, int dni, String sttcivil, String datainc, int numdespatx, String seccio) {
        super(nom, cognom, dni, sttcivil, datainc, numdespatx);
        this.seccio = seccio;
    }

    public String getSeccio() {
        return seccio;
    }

    public void setSeccio(String seccio) {
        this.seccio = seccio;
    }

    @Override
    public String toString() {
        return "PerDeServ{" +
                "seccio='" + seccio + '\'' +
                '}';
    }
}
