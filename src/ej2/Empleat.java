package ej2;

public class Empleat extends Persona {

    private String datainc;
    private int numdespatx;

    public Empleat(String nom, String cognom, int dni, String sttcivil, String datainc, int numdespatx) {
        super(nom, cognom, dni, sttcivil);
        this.datainc = datainc;
        this.numdespatx = numdespatx;
    }

    public String getDatainc() {
        return datainc;
    }

    public void setDatainc(String datainc) {
        this.datainc = datainc;
    }

    public int getNumdespatx() {
        return numdespatx;
    }

    public void setNumdespatx(int numdespatx) {
        this.numdespatx = numdespatx;
    }

    @Override
    public String toString() {
        return "Empleat{" +
                "datainc='" + datainc + '\'' +
                ", numdespatx=" + numdespatx +
                '}';
    }
}
