package ej2;

public class Professor extends Empleat {
    private String department;

    public Professor(String nom, String cognom, int dni, String sttcivil, String datainc, int numdespatx, String department) {
        super(nom, cognom, dni, sttcivil, datainc, numdespatx);
        this.department = department;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    @Override
    public String toString() {
        return "Professor{" +
                "department='" + department + '\'' +
                '}';
    }
}
