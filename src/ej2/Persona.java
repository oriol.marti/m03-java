package ej2;

public class Persona {

    private String nom;
    private String cognom;
    private int dni;
    private String sttcivil;

    public Persona(String nom, String cognom, int dni, String sttcivil) {
        this.nom = nom;
        this.cognom = cognom;
        this.dni = dni;
        this.sttcivil = sttcivil;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCognom() {
        return cognom;
    }

    public void setCognom(String cognom) {
        this.cognom = cognom;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getSttcivil() {
        return sttcivil;
    }

    public void setSttcivil(String sttcivil) {
        this.sttcivil = sttcivil;
    }
}