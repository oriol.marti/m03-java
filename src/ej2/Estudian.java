package ej2;

public class Estudian extends Persona {
    private String curs;
    private int anycurs;

    public Estudian(String nom, String cognom, int dni, String sttcivil, String curs, int anycurs) {
        super(nom, cognom, dni, sttcivil);
        this.curs = curs;
        this.anycurs = anycurs;
    }

    public String getCurs() {
        return curs;
    }

    public void setCurs(String curs) {
        this.curs = curs;
    }

    public int getAnycurs() {
        return anycurs;
    }

    public void setAnycurs(int anycurs) {
        this.anycurs = anycurs;
    }

    @Override
    public String toString() {
        return "estudian{" +
                "curs='" + curs + '\'' +
                ", anycurs=" + anycurs +
                '}';
    }
}
