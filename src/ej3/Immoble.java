package ej3;

public class Immoble {
    private String adreça;
    private int metres2;
    private String estat;
    private int preu;
    private int edat;

    public Immoble(String adreça, int metres2, String estat, int preu, int edat) {
        this.adreça = adreça;
        this.metres2 = metres2;
        this.estat = estat;
        this.preu = preu;
        this.edat = edat;
    }

    public String getAdreça() {
        return adreça;
    }

    public void setAdreça(String adreça) {
        this.adreça = adreça;
    }

    public int getMetres2() {
        return metres2;
    }

    public void setMetres2(int metres2) {
        this.metres2 = metres2;
    }

    public String getEstat() {
        return estat;
    }

    public void setEstat(String estat) {
        this.estat = estat;
    }

    public int getPreu() {
        return preu;
    }

    public void setPreu(int preu) {
        this.preu = preu;
    }

    public int getEdat() {
        return edat;
    }

    public void setEdat(int edat) {
        this.edat = edat;
    }

    public void calculatorPreu(){
        if (this.edat < 15){
            double porcentaje = 1/100;
            this.setPreu((int) (this.preu+(this.getPreu()*porcentaje)));
        }else if (this.edat  > 15){
            double porcentaje = 2/100;
            this.setPreu((int) (this.preu+(this.getPreu()*porcentaje)));
        }
    }
}
