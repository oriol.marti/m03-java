package ej3;

public class Local extends Immoble {
    private int numVidreres;

    public Local(String adreça, int metres2, String estat, int preu, int edat, int numVidreres) {
        super(adreça, metres2, estat, preu, edat);
        this.numVidreres = numVidreres;
    }

    public int getNumVidreres() {
        return numVidreres;
    }

    public void setNumVidreres(int numVidreres) {
        this.numVidreres = numVidreres;
    }

    public void calpreuLocal(){
        if(this.getMetres2() > 50){
            double porcentaje = 1/100;
            this.setPreu((int) (this.getPreu()+(this.getPreu()*porcentaje)));
        }
        if(this.numVidreres <= 1){
            double porcentaje = 2/100;
            this.setPreu((int) (this.getPreu()-(this.getPreu()*porcentaje)));
        } else if(this.numVidreres <= 4){
            double porcentaje = 2/100;
            this.setPreu((int) (this.getPreu()+(this.getPreu()*porcentaje)));
        }
    }
}
