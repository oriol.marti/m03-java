package ej3;

public class pis extends Immoble{
    private int numPis;

    public pis(String adreça, int metres2, String estat, int preu, int edat, int numPis) {
        super(adreça, metres2, estat, preu, edat);
        this.numPis = numPis;
    }

    public int getNumPis() {
        return numPis;
    }

    public void setNumPis(int numPis) {
        this.numPis = numPis;
    }

    public void calpreuPis(){
        if(this.numPis >= 3){
            double porcentaje = 3/100;
            this.setPreu((int) (this.getPreu()+(this.getPreu()*porcentaje)));
        }
    }
}
